import {each} from "../src/async-lib/promise_each";
import { delay } from "../src/async-lib/promise_delay";
import { random } from "../src/async-lib/promise_utils";

import { expect } from "chai";

describe(`Promise each tests`, () => {
    it(`should be a function`, () => {
        expect(each).to.be.a("function");
    });
    it(`test the promise each method`, async() => {
        let run = async () => {
            const result: string[] = [];
            await each("ela", async char => {
                const c = (await char) as string;
                await delay(random(100,500));
                result.push(c.toUpperCase());
            })
            return result;
        };
        expect(await run()).to.eql(["E","L","A"]);
    });
});