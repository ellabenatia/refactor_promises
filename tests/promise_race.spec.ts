import {race,some} from "../src/async-lib/promise_race";
import {delay} from "../src/async-lib/promise_delay";
import { expect } from "chai";

describe("The race function", () => {
    context(`#race`, ()=> {
        it(`should exist`, () => {
            expect(race).to.be.a("function");
        })
        it("get the first item that comeback", async () => {
            const delayMsg = async (str: string, ms: number) => {
                await delay(ms);
                return str;
            };
            const promise1 = delayMsg("1", 2000);
            const promise2 = delayMsg("2", 1000);
            const promise3 = delayMsg("3", 3000);
           let actual = await race([promise1,promise2,promise3]);
            expect(actual).to.eql("2");
        });
    });
});
describe("The some function", () => {
    context(`#some`, ()=> {
        it(`should exist`, () => {
            expect(some).to.be.a("function");
        })
        it("get x  items that comeback", async () => {
            const delayMsg = async (str: string, ms: number) => {
                await delay(ms);
                return str;
            };
            const promise1 = delayMsg("1", 2000);
            const promise2 = delayMsg("2", 1000);
            const promise3 = delayMsg("3", 3000);
           let actual = await some([promise1,promise2,promise3],2);
            expect(actual).to.eql(["2","1"]);
        });
    });
});
