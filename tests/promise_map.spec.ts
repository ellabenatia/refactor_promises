import {mapParallel,mapSeries} from "../src/async-lib/promise_map";
import { delay } from "../src/async-lib/promise_delay";
import { random } from "../src/async-lib/promise_utils";

import { expect } from "chai";
describe("Promise mapParallel tests", () => {
    // Applies only to tests in this describe block
    it("it is a function", () => {
      expect(mapParallel).to.be.a("function");
    });
    it("mapParallel", async () => {
        const promise1 = Promise.resolve(3);
        const promise2 = Promise.resolve(33);
        const promise3 = Promise.resolve(333);
        const x= await mapParallel([ promise1, promise2, promise3 ],async (item) => {
            await delay(random(100,100) );
            return (await item)+1; // Modify each item in the iterable
        })
        expect(x).to.be.eql([4,34,334]);
        });
  });
describe("Promise mapSeries tests", () => {
// Applies only to tests in this describe block
it("it is a function", () => {
    expect(mapSeries).to.be.a("function");
});
it("mapSeries", async () => {
    const promise1 = Promise.resolve(3);
    const promise2 = Promise.resolve(33);
    const promise3 = Promise.resolve(333);
    const x= await mapSeries([ promise1, promise2, promise3 ],async (item) => {
        await delay( random(100,100) );
        return await item+1; // Modify each item in the iterable
    })
    expect(x).to.be.eql([4,34,334]);
    });
});