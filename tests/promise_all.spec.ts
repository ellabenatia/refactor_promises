 import {all} from "../src/async-lib/promise_all";
 import {delay} from "../src/async-lib/promise_delay";
 import { expect } from "chai";

describe("The all function", () => {
    context(`#all`, ()=> {
        it(`should exist`, () => {
            expect(all).to.be.a("function");
        })
        it("array consist only promises", async () => {
            const delayMsg = async (str: string, ms: number) => {
                await delay(1000);
                return str;
            };
            const promise1 = delayMsg("1", 1000);
            const promise2 = delayMsg("2", 1000);
            const promise3 = delayMsg("3", 1000);
           let actual = await all([promise1,promise2,promise3]);
            expect(actual).to.eql(["1","2","3",]);
        });
    });
});
