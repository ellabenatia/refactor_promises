  
import {reduce} from "../src/async-lib/promise_reduce";
import { expect } from "chai";

describe("The reduce functions", () => {
   context(`#reduce`, ()=> {
       it(`should exist`, () => {
           expect(reduce).to.be.a("function");
       });
       it("array consist only promises", async () => {
        let cb = async function (total:number,num:number):Promise<number>{
                 return total + num;
                 };
          let actual = await reduce([1,2,3,4,5,6],cb);
           expect(actual).to.be.equal(21);
       });
   });

});
