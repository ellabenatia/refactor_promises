import {props} from "../src/async-lib/promise_props";
import {delay} from "../src/async-lib/promise_delay";
import { expect } from "chai";

describe("The props function", () => {
   context(`#props`, ()=> {
       it(`should exist`, () => {
           expect(props).to.be.a("function");
       });
       it("array consist only promises", async () => {
           const delayMsg = async (str: string, ms: number) => {
               await delay(1000);
               return str;
           };
           const promise1 = delayMsg("1", 1000);
           const promise2 = delayMsg("2", 1000);
           const promise3 = delayMsg("3", 1000);
          let actual = await props({promise1,promise2,promise3});
           expect(actual).to.eql({"promise1":"1","promise2":"2","promise3":"3"});
       });
   });
});
