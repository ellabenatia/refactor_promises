import {filterSeries, filterParallel} from "../src/async-lib/promise_filter";
import { expect } from "chai";

describe("The filter functions", () => {
   context(`#filter filterSeries`, ()=> {
       it(`should exist`, () => {
           expect(filterSeries).to.be.a("function");
       });
       it("array consist only promises", async () => {
        let cb = async function (char:string):Promise<boolean>{
                    return /^[A-Za-z]+$/.test(char); 
           };
          let actual = await filterSeries('G<4!e3ro0ni1mo',cb);
           expect(actual).to.eql(["G","e","r","o","n","i","m","o"]);
       });
   });
   context(`#filter filterSeries`, ()=> {
    it(`should exist`, () => {
        expect(filterParallel).to.be.a("function");
    });
    it("array consist only promises", async () => {
     let cb = async function (char:string):Promise<boolean>{
                 return /^[A-Za-z]+$/.test(char); 
        };
       let actual = await filterParallel('G<4!e3ro0ni1mo',cb);
        expect(actual).to.eql(["G","e","r","o","n","i","m","o"]);
    });
});
});
