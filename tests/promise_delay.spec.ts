import {delay} from "../src/async-lib/promise_delay";
import { expect } from "chai";

describe("The delay function", () => {
   context("#delay", ()=> {
       it("should exist", () => {
           expect(delay).to.be.a("function");
       });
       it(`should wait some ms and action resolve function`, async() => {
        const ms =4000;
            const startMs = Date.now();
            await delay(ms);
            const curMs = Date.now();
            // console.log(curMs- startMs)
            expect(curMs - startMs).to.gte(ms);

    });
   });
});
