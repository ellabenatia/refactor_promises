    // you have to use the Promise object
    export async function race(promises: Promise<unknown>[]):Promise<unknown>{
        return await new Promise((resolve)=>{
            promises.forEach(async p => {
                resolve(await p);
            });
        });
    }
//--------------------------------------------------
    // same here - you have to use the Promise object
export async function some(promises: Promise<unknown>[],num:number):Promise<unknown[]>{
    const results:unknown[] = [];
    return await new Promise((resolve)=>{
        promises.forEach(async p => {
            results.push(await p);
            if(results.length === num) resolve(results); 
        });
    });
}