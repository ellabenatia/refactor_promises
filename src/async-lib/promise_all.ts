export async function all(promises: Promise<any>[]): Promise<any[]> {
    const results = [];
    for (const p of promises) {
        results.push(await p);
    }
    return results;
}
