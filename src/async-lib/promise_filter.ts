export async function filterSeries(
    iterable: Iterable<any>,
    cb: (item: any) => Promise<boolean>
): Promise<Iterable<any>> {
    const results = [];
    for (const item of iterable) {
        if ((await cb(item)) === true) results.push(item);
    }
    return results;
}
//--------------------------------------------------

export async function filterParallel(
    iterable: Iterable<any>,
    cb: (item: any) => Promise<boolean>
): Promise<Iterable<any>> {
    const results = [];
    const pending = Array.from(iterable, (item) => cb(item));

    for (const [i, p] of Object.entries(iterable)) {
        if ((await pending[Number(i)]) === true)
        results.push(p);
    }
    return results;
}

//--------------------------------------------------
