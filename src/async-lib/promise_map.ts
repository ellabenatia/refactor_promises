//--------------------------------------------------

export async function mapParallel(
    iterable: any[],
    cb: (item: any) => Promise<any>
): Promise<(any[]|string)> {
    const results = [];
    const pending = Array.from(iterable, (item) => cb(item));
    for (const p of pending) {
        results.push(await p);
    }
    return results;
}

//--------------------------------------------------

export async function mapSeries(
    iterable: Iterable<any>,
    cb: (item: any) => Promise<any>
): Promise<Iterable<any>> {
    const results = [];
    for (const item of iterable) {
        results.push(await cb(item));
    }
    return results;
}

//--------------------------------------------------
