interface prop_obj {
    [index: string]: unknown;
}

export async function props(promisesObj: prop_obj): Promise<prop_obj> {
    const results: prop_obj = {};
    for (const key in promisesObj) {
        results[key] = await promisesObj[key];
    }
    return results;
}
