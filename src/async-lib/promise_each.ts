export async function each(
    iterable: any[]|string,
    cb: (item: any) => any
): Promise<any[]|string> {
    for (const item of iterable) {
        await cb(item);
    }
    return iterable;
}
