export async function reduce( iterable: any[],
    cb: (aggregator : any, item: any) => Promise<any>, initial?:any){

    let aggregator = initial===undefined?iterable[0]:initial;
    let start=0;
     if(aggregator===iterable[0]){
            start++;
        }
    for (let i=start; i<iterable.length;i++) {
        aggregator = await cb(aggregator,iterable[i]);
    }
    return aggregator;
}
