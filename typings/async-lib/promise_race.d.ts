export declare function race(promises: Promise<unknown>[]): Promise<unknown>;
export declare function some(promises: Promise<unknown>[], num: number): Promise<unknown[]>;
