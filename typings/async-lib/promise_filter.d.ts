export declare function filterSeries(iterable: Iterable<any>, cb: (item: unknown) => Promise<boolean>): Promise<Iterable<any>>;
export declare function filterParallel(iterable: unknown[], cb: (item: unknown) => Promise<boolean>): Promise<(unknown[] | string)>;
