interface prop_obj {
    [index: string]: unknown;
}
export declare function props(promisesObj: prop_obj): Promise<prop_obj>;
export {};
