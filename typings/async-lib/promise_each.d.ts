export declare function each(iterable: unknown[], cb: (item: unknown) => unknown): Promise<unknown[]>;
