export declare function reduce(iterable: unknown[], cb: (aggregator: unknown, item: unknown) => Promise<unknown>, initial: unknown): Promise<unknown>;
