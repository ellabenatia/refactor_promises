export declare function mapParallel(iterable: unknown[], cb: (item: unknown) => Promise<unknown>): Promise<(unknown[] | string)>;
export declare function mapSeries(iterable: Iterable<any>, cb: (item: any) => Promise<any>): Promise<Iterable<any>>;
